import Sequelize from 'sequelize';
import config from './database.json';

export const db = {};

Object.keys(config.database).map((key) => {
  let database = config.database[key];

  db[key] = new Sequelize(
    database.database,
    database.username,
    database.password,
    {
      ...database.patch,
    }
  );
  db[key]
    .authenticate()
    .then(() => {
      console.log(`Conexion establecida con ${key}.`);
    })
    .catch((err) => {
      console.error('Error en la conexion con la base de datos:', err);
    });
});

// crear moedelos de la base de datos
// sequelize-auto -o "./src/db/saime" -d saime -h localhost -u postgres -p 5432 -x root -e postgres -s saime
