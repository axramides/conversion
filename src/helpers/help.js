import axios from 'axios';

import { MONEDAS } from '../models/';
export function Res(ok, message) {
  return {
    ok: ok,
    data: {
      message,
    },
  };
}
/* funcion para obtener los precios de su api correspondiente y las que no tengan api de la base de datos */
export const PrecioApi = async (nombre) => {
  let response;
  switch (nombre) {
    case 'dash':
    case 'bitcoin':
    case 'ethereum':
      try {
        response = await axios.get(
          '  https://api.coingecko.com/api/v3/simple/price',
          {
            params: { ids: nombre, vs_currencies: 'usd' },
          }
        );

        const precio = Object.keys(response.data).map((key) => {
          return response.data[key].usd;
        });
        return precio[0];
      } catch (error) {
        console.log('error en la conexion con la api:', error.response.data);
        return;
      }
    case 'ptr':
      try {
        response = await axios.post(
          'https://petroapp-price.petro.gob.ve/price',
          {
            coins: ['PTR'],
            fiats: ['USD'],
          }
        );

        return response.data.data.PTR.USD;
      } catch (error) {
        console.log('error en la conexion con la api:', error.code);
        return;
      }

    case 'bolivar':
    case 'dolar':
    case 'euro':
      try {
        const response = await MONEDAS.findOne({
          where: {
            moneda: nombre,
          },
          raw: true,
        });

        return response.precio;
      } catch (error) {
        console.log(error);
      }

    default:
      throw new Error('moneda no encontradas');
  }
};
