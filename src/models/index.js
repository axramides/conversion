import { db } from '../config/database';
import Sequelize from 'sequelize';
import monedas from './monedas/monedas';

const MONEDAS = monedas(db.moneda, Sequelize);

export { MONEDAS };
