import { Router } from 'express';
const router = Router();
/* Controladores */
import {
  convertir,
  getMonedas,
  getMoneda,
  putMoneda,
} from '../controllers/monedas.controller';

router.route('/').get(getMonedas);
router.route('/moneda').get(getMoneda).put(putMoneda);
router.route('/conversion').post(convertir);

export default router;
