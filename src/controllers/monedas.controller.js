import '../config/config';
import { Res, PrecioApi } from '../helpers/help.js';
import { isUndefined, isEmpty, isInteger } from 'lodash';
import { MONEDAS } from '../models/';
import { Op } from 'sequelize';
import Moneda from '../classes/moneda';

/* @route POST api/monedas/convertir
 @desc Convertir una monada a otra moneda pasandole los datos por json mediante el body del request
monedas disponibles : bolivar, euro, dolar, ptr (petro), bitcoin, ethereum, dash
ejemplo
{
  moneda: bolivar,
  monto:100000,
  moneda:dash
}*/

export const convertir = async (req, res) => {
  try {
    /* Json del request */
    const { monto, moneda, moneda2 } = req.body;

    /* Obtenemos el precio del api o de la base de datos */
    var precio1 = await PrecioApi(moneda);
    var precio2 = await PrecioApi(moneda2);

    /* si no podemos obtener el precio de la api hacemos un consulta en la base de datos */
    if (isUndefined(precio1) || isUndefined(precio2)) {
      const precioDatabase = await MONEDAS.findAll({
        where: {
          moneda: {
            [Op.in]: [moneda, moneda2],
          },
        },
        raw: true,
      });
      /* si la moneda no se encuentra en la base de datos, no esta disponible */
      if (isEmpty(precioDatabase)) {
        return res
          .status(404)
          .json(Res(false, 'la moneda no se encuentra disponible'));
      }

      precio1 = precioDatabase.find((nombre) => nombre.moneda == moneda).precio;
      precio2 = precioDatabase.find((nombre) => nombre.moneda == moneda2)
        .precio;
    } else {
      const precios = [
        { moneda: moneda, precio: precio1 },
        { moneda: moneda2, precio: precio2 },
      ];
      for (let i = 0; i < precios.length; i++) {
        await MONEDAS.update(
          { precio: precios[i].precio },
          {
            where: { moneda: precios[i].moneda },
          }
        );
      }
    }

    /* guardamos la moneda y monto que queremos convertir en un nuevo objeto  */
    const convertir = new Moneda(moneda, monto, precio1, moneda2, precio2);

    /* convertimos de moneda a moneda */
    convertir.monedaMoneda();

    /* limitamos la cantidad de decimales a 8 dependiendo de si es decimal o entero */
    const conversion = {
      monenda: convertir.moneda2,
      monto: !isInteger(convertir.conversion)
        ? convertir.conversion.toFixed(8)
        : convertir.conversion,
    };

    return res.json(Res(true, conversion));
  } catch (err) {
    console.log(err);
    return res.status(500).json(Res(false, err));
  }
};

// @route GET api/monedas
// @desc Obtener todas los precios en la base de datos de las monedas

export const getMonedas = async (req, res) => {
  try {
    const response = await MONEDAS.findAll({});
    return res.json(Res(true, response));
  } catch (err) {
    console.log(err);
    return res.status(500).json(Res(false, err));
  }
};

// @route GET api/monedas/moneda?nombre="moneda":
// @desc Obtener el precio de la moneda por su nombre pasandole como parametros a la ruta el nombre de la moneda: nombre="nombre moneda"

export const getMoneda = async (req, res) => {
  try {
    const moneda = req.query.nombre;
    const response = await MONEDAS.findOne({
      where: { moneda },
    });
    return res.json(Res(true, response));
  } catch (err) {
    console.log(err);
    return res.status(500).json(Res(false, err));
  }
};

// @route PUT api/monedas/moneda?nombre="moneda":
// @desc actualizar el precio de la moneda por su nombre pasandole como parametros a la ruta el nombre de la moneda: nombre="nombre moneda"

export const putMoneda = async (req, res) => {
  try {
    const moneda = req.query.nombre;
    const data = req.body;
    console.log(moneda);
    await MONEDAS.update(data, {
      where: { moneda },
    });
    return res.json(Res(true, `precio del ${moneda} modificado`));
  } catch (err) {
    console.log(err);
    return res.status(500).json(Res(false, err));
  }
};
