/* Clase de la moneda para efectuar la conversion  */
export default class Moneda {
  constructor(moneda, monto, valor, moneda2, valor2) {
    (this.moneda = moneda),
      (this.monto = monto),
      (this.valor = valor),
      (this.moneda2 = moneda2),
      (this.valor2 = valor2),
      (this.conversion = 0);

    /* si la moneda es bolivar  convertimos el valor a 1 bolivar = 1 dolar */
    if (this.moneda == 'bolivar') {
      this.valor = 1 / this.valor;
    }
    if (this.moneda2 == 'bolivar') {
      this.valor2 = 1 / this.valor2;
    }
  }

  monedaMoneda() {
    this.conversion = (this.monto * this.valor) / this.valor2;
  }
}
