import express, { json } from 'express';
import cors from 'cors';

/*RUTAS */
import moneda from './routes/moneda.routes';

/* Headers */
const app = express();
app.use(function (req, res, next) {
  res.setHeader('Access-Control-Allow-Origin', '*');
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE');
  res.setHeader(
    'Access-Control-Allow-Headers',
    'X-Requested-With,content-type'
  );
  res.setHeader('Access-Control-Allow-Credentials', true);
  next();
});
//midelewares
app.use(json());
app.use(cors());

/* RUTAS */
app.use('/api/monedas', moneda);

export default app;
